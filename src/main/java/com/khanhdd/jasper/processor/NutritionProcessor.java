package com.khanhdd.jasper.processor;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.khanhdd.jasper.model.Food;
import com.khanhdd.jasper.model.MacroNutrient;
import com.khanhdd.jasper.model.Nutrition;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Component
public class NutritionProcessor {
    private static JRBeanCollectionDataSource getFoodDateSource() {
        List<Food> foodList = new ArrayList<>();
        Food banana = new Food("banana", "breakfast", 0, 28, 1);
        Food avocado = new Food("avocado", "breakfast", 22, 13, 3);
        Food milk = new Food("milk", "breakfast", 8, 12, 8);
        Food chicken = new Food("chicken", "lunch", 2, 0, 26);
        Food rice = new Food("rice", "lunch", 0, 45, 26);
        Food egg = new Food("egg", "lunch", 5, 0, 6);
        Food potato = new Food("potato", "lunch", 5, 37, 4);
        Food oats = new Food("oats", "dinner", 5, 51, 13);

        foodList.add(banana);
        foodList.add(avocado);
        foodList.add(milk);
        foodList.add(chicken);
        foodList.add(rice);
        foodList.add(egg);
        foodList.add(potato);
        foodList.add(oats);

        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(foodList);
        return dataSource;
    }

    private static Map getFoodParameter() {
        Map<String, Object> foodParameter = new HashMap<>();
        foodParameter.put("foodDataset", getFoodDateSource());
        return foodParameter;
    }

    private static JasperReport getFoodReport() {
        String filePath = new File("src/main/resources/templates").getAbsolutePath() + "\\food_nutrition.jrxml";
        JasperReport report = null;
        try {
            report = JasperCompileManager.compileReport(filePath);
        } catch (JRException e) {
            throw new RuntimeException(e);
        }
        return report;
    }

    public ByteArrayOutputStream generateReport() throws JRException, DocumentException, IOException {
        String filePath = new File("src/main/resources/templates").getAbsolutePath() + "\\NutritionReport.jrxml";
        Nutrition protein = new Nutrition("Protein", 62, 83, "g");
        Nutrition carbohydrates = new Nutrition("Carbohydrates", 180, 206, "g");
        Nutrition fiber = new Nutrition("Fiber", 20, 38, "g");
        Nutrition sugars = new Nutrition("Sugars", 68, 62, "g");
        Nutrition fat = new Nutrition("Fat", 60, 55, "g");
        Nutrition cholesterol = new Nutrition("Cholesterol", 84, 300, "g");
        Nutrition sodium = new Nutrition("Sodium", 2200, 2300, "g");
        Nutrition potassium = new Nutrition("Potassium", 2000, 3500, "g");
        Nutrition calcium = new Nutrition("Calcium", 62, 100, "g");
        Nutrition iron = new Nutrition("Iron", 38, 100, "g");

        List<Nutrition> nutritionList = new ArrayList<>();
        nutritionList.add(protein);
        nutritionList.add(carbohydrates);
        nutritionList.add(fiber);
        nutritionList.add(sugars);
        nutritionList.add(fat);
        nutritionList.add(cholesterol);
        nutritionList.add(sodium);
        nutritionList.add(potassium);
        nutritionList.add(calcium);
        nutritionList.add(iron);
        List<MacroNutrient> macroNutrients = new ArrayList<>();
        nutritionList.stream().forEach(
                i -> {
                    if (i.getNutritionName().equals("Protein") || i.getNutritionName().equals("Carbohydrates") || i.getNutritionName().equals("Fat")) {
                        MacroNutrient macroNutrient = new MacroNutrient();
                        macroNutrient.setMacroNutrientName(i.getNutritionName());
                        macroNutrient.setMacroNutrientValue(i.getGoal());
                        macroNutrients.add(macroNutrient);
                    }
                }
        );

        JRBeanCollectionDataSource nutritionDataSource = new JRBeanCollectionDataSource(nutritionList);
        JRBeanCollectionDataSource macroDataSource = new JRBeanCollectionDataSource(macroNutrients);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("firstName", "John");
        parameters.put("lastName", "Locke");
        parameters.put("dob", "10/03/1998");
        parameters.put("age", 25);
        parameters.put("nutritionDataset", nutritionDataSource);
        parameters.put("macroNutrientDataSet", macroDataSource);
        parameters.put("foodReport", getFoodReport());
        parameters.put("foodParameter", getFoodParameter());

        JasperReport report = JasperCompileManager.compileReport(filePath);
        JasperPrint print = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        JRPdfExporter exporter = new JRPdfExporter();
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        configuration.setCompressed(true);
        exporter.setConfiguration(configuration);
        exporter.setExporterInput(new SimpleExporterInput(print));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
        exporter.exportReport();
        ByteArrayOutputStream watermarkedStream = new ByteArrayOutputStream();
        addWatermarkToPDF(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()), watermarkedStream, "CONFIDENTIAL");

        return watermarkedStream;
    }

    // Hàm để thêm watermark vào PDF sử dụng iText
    private void addWatermarkToPDF(ByteArrayInputStream pdfInput, ByteArrayOutputStream pdfOutput, String watermarkText)
            throws IOException, DocumentException {

        PdfReader pdfReader = new PdfReader(pdfInput);
        PdfStamper pdfStamper = new PdfStamper(pdfReader, pdfOutput);

        // Tạo font và định dạng watermark
        Font font = new Font(Font.FontFamily.HELVETICA, 50, Font.BOLD, new BaseColor(200, 200, 200));
        Phrase watermark = new Phrase(watermarkText, font);

        // Lặp qua từng trang để chèn watermark
        int totalPages = pdfReader.getNumberOfPages();
        for (int i = 1; i <= totalPages; i++) {
            PdfContentByte content = pdfStamper.getUnderContent(i);

            // Thiết lập vị trí watermark (ở giữa trang) và góc xoay
            ColumnText.showTextAligned(content, Element.ALIGN_CENTER, watermark,
                    297.5f, 421, 45);  // 297.5 và 421 là tọa độ trung tâm trang A4
        }

        pdfStamper.close();
        pdfReader.close();
    }

}

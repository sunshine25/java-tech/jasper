package com.khanhdd.jasper.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MacroNutrient {
    private String macroNutrientName;
    private int macroNutrientValue;
}

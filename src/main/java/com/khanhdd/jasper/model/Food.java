package com.khanhdd.jasper.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Food {
    private String foodName;
    private String mealtime;
    private int fat;
    private int carbohydrate;
    private int protein;
}

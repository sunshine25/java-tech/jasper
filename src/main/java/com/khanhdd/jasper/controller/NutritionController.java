package com.khanhdd.jasper.controller;

import com.itextpdf.text.DocumentException;
import com.khanhdd.jasper.processor.NutritionProcessor;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
public class NutritionController {

    private final NutritionProcessor nutritionProcessor;
    @GetMapping("/report")
    public ResponseEntity getNutritionReport() throws JRException, DocumentException, IOException {
        ByteArrayOutputStream reportStream = nutritionProcessor.generateReport();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_PDF);
        return new ResponseEntity(reportStream.toByteArray(), httpHeaders, HttpStatus.OK);
    }
}
